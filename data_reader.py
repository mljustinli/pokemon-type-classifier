import csv

"""
returns index to type (0 to n mapped to pokemon type)
and type to index (pokemon type to 0 to n)

"""

no_secondary_type = "None"

types = ["Normal", "Fire", "Fighting", "Water", "Flying", "Grass", "Poison",
    "Electric", "Ground", "Psychic", "Rock", "Ice", "Bug", "Dragon", "Ghost", "Dark", "Steel",
    "Fairy", "None"]
primary_type_weights = {}

pokemon_to_primary_index = {}
pokemon_to_secondary_index = {}

def open_csv(filepath, filename):

    with open(filepath + '/' + filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                # toPrint = row[0] + " " + row[1] + " "
                # if len(row) == 3:
                #     toPrint += row[2]
                # print(toPrint)

                pokemon_name = row[0]
                type1 = row[1]
                type2 = no_secondary_type if len(row) < 3 else row[2]

                print(type1 + " " + type2)

                pokemon_to_primary_index[pokemon_name] = types.index(type1)
                pokemon_to_secondary_index[pokemon_name] = types.index(type2)

                if pokemon_name in primary_type_weights:
                    primary_type_weights[pokemon_to_primary_index[pokemon_name]] += 1
                else:
                    primary_type_weights[pokemon_to_primary_index[pokemon_name]] = 1.0

                line_count += 1
        print(f'Processed {line_count} lines.')

    # print(types)
    # print(pokemon_to_primary_index)
    # print(pokemon_to_secondary_index)

    # return index_to_type, type_to_index

