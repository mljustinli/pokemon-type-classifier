import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
import sys
import random
import data_reader as dr

from data_reader import open_csv
from open_images import open_images

# can print more lines
# np.set_printoptions(threshold=sys.maxsize)

# https://www.kaggle.com/vishalsubbiah/pokemon-images-and-types#

data_dir = 'pokemon-images-and-types'
csv_name = 'pokemon.csv'
image_dir_name = 'images'

# have to open csv first or else it doesn't know the names and types
open_csv(data_dir, csv_name)
all_data = open_images(data_dir, image_dir_name, csv_name)

random.shuffle(all_data)

portion = int(len(all_data) * (8 / 10))
training_data = all_data[:portion]
test_data = all_data[portion:]

training_x, training_y, training_names = zip(*training_data)
training_x = np.asarray(training_x)
training_y = np.asarray(training_y)

test_x, test_y, test_names = zip(*test_data)
test_x = np.asarray(test_x)
test_y = np.asarray(test_y)

# Training Time

import tensorflow as tf

#https://www.dlology.com/blog/one-simple-trick-to-train-keras-model-faster-with-batch-normalization/

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten())

# model.add(tf.keras.layers.Dense(45, use_bias='False'))
# model.add(tf.keras.layers.Dense(25, use_bias='False'))
# model.add(tf.keras.layers.Dense(128, use_bias='False'))
model.add(tf.keras.layers.Dense(400, use_bias='False'))
model.add(tf.keras.layers.Dense(128, use_bias='False'))
model.add(tf.keras.layers.Dense(400, use_bias='False'))
# model.add(tf.keras.layers.Dense(25, use_bias='False'))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Activation("relu"))

# model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
# model.add(tf.keras.layers.Dense(200, activation=tf.nn.relu))
# - 1 because types has a None which we don't want for the primary type training
model.add(tf.keras.layers.Dense(len(dr.types) - 1, activation=tf.nn.softmax))

model.compile(optimizer="adam", loss="sparse_categorical_crossentropy",
    metrics=['accuracy'])

model.fit(training_x, training_y, epochs = 10)
# model.fit(training_x, training_y, epochs=5, class_weight = dr.primary_type_weights)

val_loss, val_acc = model.evaluate(test_x, test_y)
print(val_loss, val_acc)

predictions = model.predict([test_x])

plt.figure(figsize=(10, 10))
for i in range(25):
    predicted = np.argmax(predictions[i])
    plt.subplot(5, 5, i + 1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(cv2.cvtColor(test_x[i], cv2.COLOR_BGR2RGB))
    # test_names[i] + " " +
    plt.xlabel(dr.types[predicted] + " | " + dr.types[test_y[i]])
plt.show()



