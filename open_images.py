import os
import cv2

import data_reader as dr
from data_reader import open_csv

"""
returns training data (with type labels), type list

- training data is an array of [image, index of primary type in type list]
- type list is an array of pokemon types where 0 is ... whatever the first one is
doesn't account for double types, which is why we have two
"""
def open_images(data_dir, image_dir_name, csv_name):
    training_data = []
    type_list = []
    training_labels = []

    # open_csv(data_dir, csv_name)

    # print(index_to_type)
    # print("as;dlfkajsd;lfkjas;ldfkjas")
    # print(type_to_index)

    path = os.path.join(data_dir, image_dir_name)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_COLOR)
        norm_img_array = cv2.normalize(img_array, None, alpha=0, beta = 1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

        # return image array with rgb 0 to 255
        # training_data.append([img_array, 1])

        # return image array with rgb 0 to 1
        training_data.append([norm_img_array,
            dr.pokemon_to_primary_index[img[:len(img) - 4]],
            img[:len(img) - 4]])

    return training_data
